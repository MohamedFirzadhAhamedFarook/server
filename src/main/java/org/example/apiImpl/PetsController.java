package org.example.apiImpl;

import java.util.List;
import org.example.api.PetsApi;
import org.example.model.Pet;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class PetsController implements PetsApi {

  public ResponseEntity<Void> createPets() {
    return new ResponseEntity<>(HttpStatus.CREATED);
  }


  public ResponseEntity<List<Pet>> listPets(Integer limit) {
    return new ResponseEntity<>(HttpStatus.ACCEPTED);

  }


  public ResponseEntity<Pet> showPetById(String petId) {

    return new ResponseEntity<>(HttpStatus.ACCEPTED);

  }
}
